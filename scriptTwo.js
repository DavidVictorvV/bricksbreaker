var play = false;
var lives = 3;
var livesBackUp = lives;
var isAuto = 0;
var points = 0;
var getPoints = 0;
var pointsLeft;
var bricksWall = [];
var canvasWidth = window.innerWidth;
var canvasHeight = window.innerHeight;
var levelNr = 1;
var maxLevel = 11;
//pallet proprieties
var pallet;
var palletSize = 150;
var enemyBrickSize = 100;
var palletHeight = canvasHeight - 80 - ((canvasHeight - 80) % 10);
//ball proprieties
var ball;
var ballSpeed = 10;
var size = 50;
var smileyFaceStepX;
var smileyFaceStepY;

function setup() {
  createCanvas(canvasWidth, canvasHeight);

  defaultPos();
  levelPlay();
  playButton = createButton("Play");
  playButton.position(canvasWidth / 2 - 20, canvasHeight / 2);
  playButton.mousePressed(playPause);

  resetButton = createButton("Reset");
  resetButton.position(canvasWidth / 2 - 25, canvasHeight / 2 + 20);
  resetButton.mousePressed(pressReset);
  resetButton.hide();
}

function draw() {
  background("lightBlue");
  ball = new smileyFace(smileyFaceStepX, smileyFaceStepY, size);
  // autoPlay();
  palett();
  heartsLeft();
  ifWin();
  showStats();
  ballBounceofPallet();
  ballBoundries();

  if (play || lives == 0) {
    playButton.hide();
  } else if (lives == 0) {
    resetButton.show();
  } else if (levelNr <= maxLevel) playButton.show();
  bricksWall.forEach((row) => {
    row.forEach((brick) => {
      fill(brick.getColor());
      brick.drawBrick();
    });
  });

  checkBrick();
  moveBall();
}

function lvlEnd() {
  lives = livesBackUp;
  points = 0;
  getPoints = 0;
  levelNr++;
  ballSpeed++;
  bricksWall = [];
  levelPlay();
}

function pressReset() {
  bricksWall = [];
  resetButton.hide();
  playButton.show();
  lives = livesBackUp;
  points = 0;
  getPoints = 0;
  levelNr = 1;
  defaultPos();
  levelPlay();
}

class enemyBrick {
  constructor(x, y, w, h, color) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.hit = false;
    this.color = color;
  }

  drawBrick() {
    if (!this.hit) {
      rect(this.x, this.y, this.w, this.h);
    }
  }

  setHit() {
    this.hit = true;
  }

  getColor() {
    return this.color;
  }
}

function bricksWallRow(y, brickSize) {
  bricksWallCurr = [];
  level();
  bricksPosX = Math.floor(random(20, 40));
  for (j = 0; j < canvasWidth; j++) {
    brickSize = 40 + random(a, b);
    randomColor =
      "#" + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6);
    if (bricksPosX < canvasWidth - brickSize - 20) {
      bricksWallCurr.push(
        new enemyBrick(bricksPosX, y, brickSize, 15, randomColor)
      );
      bricksPosX += brickSize * 1.5;
    }
  }
  bricksWall.push(bricksWallCurr);
  getPoints += bricksWallCurr.length;
  pointsLeft = getPoints;
}

class smileyFace {
  constructor(x, y, r) {
    this.x = x;
    this.y = y;
    this.r = r / 2;
    fill("yellow");
    circle(x, y, r);
    fill("white");
    circle(x + r * 0.17, y - 10, (2 / 10) * r);
    circle(x - r * 0.15, y - 10, (2 / 10) * r);
    fill("green");
    circle(x + r * 0.17, y - 10, (1 / 10) * r);
    circle(x - r * 0.15, y - 10, (1 / 10) * r);
    fill(0);
    circle(x + r * 0.17, y - 10, (4 / 100) * r);
    circle(x - r * 0.15, y - 10, (4 / 100) * r);
    noFill();
    arc(x, y, r * 0.6, r * 0.6, 0.5, PI / 1.2);
  }
}
function palett() {
  fill("blue");
  x = isAuto ? smileyFaceStepX - palletSize / 2 : mouseX - palletSize / 2;
  if (x + palletSize / 2 <= palletSize / 2) {
    x = 0;
  } else if (x + palletSize / 2 >= canvasWidth - palletSize / 2) {
    x = canvasWidth - palletSize;
  }

  pallet = rect(x, palletHeight, palletSize, 15);
}

function checkBrick() {
  bricksWall.forEach((row) => {
    row.forEach((brick, j, arr) => {
      if (RectCircleColliding(ball, brick) != false) {
        if (RectCircleColliding(ball, brick) != "side")
          smileyFaceStepTurnY *= -1;
        else {
          smileyFaceStepTurnX *= -1;
        }
        arr.splice(j, 1);
        points++;
        pointsLeft--;
      }
    });
  });
}

function heart(x, y, size) {
  beginShape();
  vertex(x, y);
  bezierVertex(x - size / 2, y - size / 2, x - size, y + size / 3, x, y + size);
  bezierVertex(x + size, y + size / 3, x + size / 2, y - size / 2, x, y);
  endShape(CLOSE);
}

function defaultPos() {
  smileyFaceStepTurnX =
    ballSpeed * (Math.floor(Math.random() * Math.floor(2)) == 1 ? 1 : -1);
  smileyFaceStepTurnY = -ballSpeed;
  // * (Math.floor(Math.random() * Math.floor(2)) == 1 ? 1 : -1)
  smileyFaceStepX = Math.floor(canvasWidth / 2);
  smileyFaceStepY = Math.floor(canvasHeight * 0.8 - ((canvasHeight * 0.8) % 5));
}

function playPause() {
  if (play) {
    play = false;
  } else play = true;
}

function autoPlay() {
  isAuto = 1;
}

function showStats() {
  textSize(20);
  text("Points: " + points, 20, 35);
  text("Bricks left: " + pointsLeft, 20, 55);
  text("LVL: " + levelNr, canvasWidth / 2, 35);
}

function ifWin() {
  if (points == getPoints && levelNr == maxLevel) {
    textSize(32);
    textAlign(CENTER);
    text("YOU WIN", canvasWidth / 2, canvasHeight / 3 - size / 1.5);
    play = false;
    playButton.hide();
    resetButton.show();
  }
  if (points == getPoints && levelNr < maxLevel) {
    play = false;
    defaultPos();
    if (smileyFaceStepY > canvasHeight / 2) {
      lvlEnd();
    }
  }

  if (!play && lives == 0) {
    textSize(32);
    textAlign(CENTER);
    text("THE END", canvasWidth / 2, canvasHeight / 1.5 - size / 1.5);
    resetButton.show();
  }
  if (smileyFaceStepY >= canvasHeight) {
    lives--;
    defaultPos();
    playPause();
  }
}

function moveBall() {
  if (play) {
    smileyFaceStepX += smileyFaceStepTurnX;
    smileyFaceStepY += smileyFaceStepTurnY;
  }
}

function ballBoundries() {
  // ball boundries/ bounce for the walls
  if (
    smileyFaceStepX >= canvasWidth - size / 2 ||
    smileyFaceStepX <= size / 2
  ) {
    smileyFaceStepTurnX *= -1;
  }
  if (smileyFaceStepY <= size / 2) {
    smileyFaceStepTurnY *= -1;
  }
}

function heartsLeft() {
  fill("red");
  heartSize = 20;
  pos = 0;
  for (i = 0; i < lives; i++) {
    pos += heartSize * 1.5;
    heart(canvasWidth - pos, canvasHeight / 40, heartSize);
  }
}

function RectCircleColliding(circle, rect) {
  var distX = Math.abs(circle.x - rect.x - rect.w / 2);
  var distY = Math.abs(circle.y - rect.y - rect.h / 2);

  if (distX > rect.w / 2 + circle.r) {
    return false;
  }
  if (distY > rect.h / 2 + circle.r) {
    return false;
  }

  if (distX <= rect.w / 2) {
    // return true;
    return "top";
  }
  if (distY <= rect.h / 2) {
    // return true;
    return "bottom";
  }

  var dx = distX - rect.w / 2;
  var dy = distY - rect.h / 2;
  return dx * dx + dy * dy <= circle.r * circle.r ? false : "side";
}

function ballBounceofPallet() {
  // //get pos of x on both sides of palett
  // mouseXPos = isAuto ? smileyFaceStepX : mouseX + palletSize / 2;
  // mouseXPosTwo = isAuto ? smileyFaceStepX : mouseX - palletSize / 2;

  // //pos of palett bounce if mouse is not on screen(left)
  // if (mouseX >= canvasWidth - palletSize) {
  //   mouseXPos = canvasWidth;
  //   mouseXPosTwo = canvasWidth - palletSize;
  // }

  // //pos of palett bounce if mouse is not on screen(right)
  // if (mouseX <= palletSize / 2) {
  //   mouseXPos = palletSize;
  //   mouseXPosTwo = 0;
  // }
  // //ball bounce on pos of x of palette
  if (
    RectCircleColliding(ball, {
      x:
        isAuto == 1
          ? smileyFaceStepX - palletSize / 2
          : mouseX - palletSize / 2,
      y: palletHeight,
      w: palletSize,
      h: 15,
    }) != false
  ) {
    smileyFaceStepTurnY *= -1;
  }
}

function levelPlay() {
  console.log();
  ran = Math.floor(random(10)) == 1 ? console.log(1) : bricksWallRow(100);
  bricksWallRow(150);
  ran = Math.floor(random(10)) == 1 ? console.log(3) : bricksWallRow(200);
  bricksWallRow(250);
  ran = Math.floor(random(10)) == 1 ? console.log(5) : bricksWallRow(300);
  //   bricksWallRow(200);
  //   bricksWallRow(300);
}

function level() {
  switch (levelNr) {
    case 1:
      a = 100;
      b = 100;
      break;
    case 2:
      a = 70;
      b = 100;
      break;
    case 3:
      a = 50;
      b = 100;
      break;
    case 4:
      a = 50;
      b = 100;
      break;
    case 5:
      a = 50;
      b = 100;
      break;
    case 6:
      a = 40;
      b = 100;
      break;
    case 7:
      a = 30;
      b = 100;
      break;
    case 8:
      a = 20;
      b = 80;
      break;
    case 9:
      a = 10;
      b = 70;
      break;
    case 10:
      a = 10;
      b = 50;
      break;
  }
}
