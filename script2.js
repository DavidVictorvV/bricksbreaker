var ball = {
  x: 0,
  y: 0,
  size: 50,
  dirX: 1,
  dirY: 1,
  speed: 5,
};
var bar = {
  x: 0,
  y: 0,
  width: 20,
  height: 20,
  distToBottom: 80,
};
var canvasVar = {
  width: window.innerWidth,
  height: window.innerHeight,
};
var play = false;
var lives = 3;
var isAuto;

function init() {
  bar.y =
    canvasVar.height -
    bar.distToBottom -
    ((canvasVar.height - bar.distToBottom) % ball.speed);
  ball.dirX = Math.floor(Math.random() * Math.floor(2)) == 1 ? 1 : -1;
  ball.dirY = Math.floor(Math.random() * Math.floor(2)) == 1 ? 1 : -1;
  ball.x = Math.floor(canvasVar.width / 2);
  ball.y = Math.floor(
    canvasVar.height / 3 - ((canvasVar.height / 3) % ball.speed)
  );
}

function setup() {
  isAuto = true;
  createCanvas(canvasVar.width, canvasVar.height);
  button = createButton("Play");
  button.position(
    canvasVar.width / 2 - button.width / 2,
    canvasVar.height / 3 + 30
  );
  button.mousePressed(playPause);
  init();

  //   console.log();
}

function draw() {
  background("lightBlue");
  textSize(32);
  text(lives, canvasVar.width - 70, 40);

  palett();
  smileyFace(ball.x, ball.y, ball.size);
  ballBoundries();
  ballBounce();
  moveBall();

  if (ball.y >= canvasVar.height) {
    lives--;

    init();
    playPause();
  }

  if (lives == 0) {
    textSize(32);

    textAlign(CENTER);

    text("THE END", canvasVar.width / 2, canvasVar.height / 3 - 50);

    button.hide();
  }
}

function defaultPos() {
  ballSpeed = 10;
  smileyFaceStepTurnX =
    ballSpeed * (Math.floor(Math.random() * Math.floor(2)) == 1 ? 1 : -1);
  smileyFaceStepTurnY =
    ballSpeed * (Math.floor(Math.random() * Math.floor(2)) == 1 ? 1 : -1);
  smileyFaceStepX = Math.floor(canvasWidth / 2);
  smileyFaceStepY = Math.floor(canvasHeight / 3 - ((canvasHeight / 3) % 5));
}

function ballBoundries() {
  if (ball.x >= canvasVar.width - 20 || ball.x <= 20) {
    ball.dirX *= -1;
  }
  if (ball.y <= 20) {
    ball.dirY *= -1;
  }
}

function moveBall() {
  if (play) {
    console.log(ball.dirX, ball.speed);
    ball.x += ball.dirX * ball.speed;
    ball.y += ball.dirY * ball.speed;
  }
}

function ballBounce() {
  //get pos of x on both sides of palett
  mouseXPos = isAuto ? ball.x : mouseX + bar.width / 2;
  mouseXPosTwo = isAuto ? ball.y : mouseX - bar.width / 2;

  //pos of palett bounce if mouse is not on screen(left)
  if (mouseX >= canvasVar.width - bar.width) {
    mouseXPos = canvasVar.width;
    mouseXPosTwo = canvasVar.width - bar.width;
  }

  //pos of palett bounce if mouse is not on screen(right)
  if (mouseX <= 75) {
    mouseXPos = 150;
    mouseXPosTwo = 0;
  }
  //ball bounce on pos of x of palette
  if (
    ball.x <= mouseXPos &&
    ball.x >= mouseXPosTwo &&
    ball.y >= canvasVar.height - bar.distToBottom - bar.height - 25 &&
    ball.y <= canvasVar.height - bar.distToBottom - bar.height - 15
  ) {
    ball.dirY *= -1;
  }
}

function autoPlay() {
  isAuto = 1;
}

function playPause() {
  if (!play) {
    button.hide();
  } else button.show();

  if (play) {
    play = false;
  } else play = true;
}

function palett() {
  fill("blue");
  x = isAuto ? ball.x - bar.width / 2 : mouseX - bar.width / 2;
  if (x + bar.width / 2 <= bar.width / 2) {
    x = 0;
  } else if (x + bar.width / 2 >= canvasVar.width - bar.width / 2) {
    x = canvasVar.width - bar.width;
  }

  bar.pallet = rect(
    x,
    canvasVar.height - bar.distToBottom - bar.height,
    bar.width,
    bar.height
  );
}

function smileyFace(x, y, size) {
  fill("yellow");
  circle(x, y, size);
  fill("white");
  circle(x + 10, y - 10, (2 / 10) * size);
  circle(x - 10, y - 10, (2 / 10) * size);
  fill("green");
  circle(x + 10, y - 10, (1 / 10) * size);
  circle(x - 10, y - 10, (1 / 10) * size);
  fill(0);
  circle(x + 10, y - 10, (4 / 100) * size);
  circle(x - 10, y - 10, (4 / 100) * size);
  fill("red");
  arc(x, y + 5, 25, 25, 0, PI);
}
