var play = false;
var lives = 3;
var livesBackUp = lives;
var isAuto = 0;
var canvasWidth = window.innerWidth;
var canvasHeight = window.innerHeight;
var points = 0;
var getPoints = 0;
var bricksWall = [];
//pallet proprieties
var pallet;
var palletSize = 150;
var enemyBrickSize = 100;
var palletHeight = canvasHeight - 80 - ((canvasHeight - 80) % 10);
//ball proprieties
var ball;
var ballSpeed = 10;
var size = 50;
var smileyFaceStepX;
var smileyFaceStepY;

function setup() {
  createCanvas(canvasWidth, canvasHeight);

  defaultPos();
  // bricksWallRow(10);
  bricksWallRow(80);
  bricksWallRow(100);
  playButton = createButton("Play");
  playButton.position(canvasWidth / 2 - 20, canvasHeight / 2);
  playButton.mousePressed(playPause);

  resetButton = createButton("Reset");
  resetButton.position(canvasWidth / 2 - 25, canvasHeight / 2 + 20);
  resetButton.mousePressed(pressReset);
  resetButton.hide();
}

function pressReset() {
  resetButton.hide();
  playButton.show();
  lives = livesBackUp;
  points = 0;
  defaultPos();
  bricksWallRow(80);
}

function draw() {
  background("lightBlue");
  ball = new smileyFace(smileyFaceStepX, smileyFaceStepY, size);
  palett();
  heartsLeft();
  if (play || lives == 0) {
    playButton.hide();
  } else if (lives == 0) {
    resetButton.show();
  } else {
    playButton.show();
  }

  textSize(20);
  text("Points: " + points, 10, 35);
  text(bricksWall.length, 10, 55);

  if (points == getPoints) {
    textSize(32);
    textAlign(CENTER);
    text("YOU WIN", canvasWidth / 2, canvasHeight / 3 - size / 1.5);
    play = false;
    playButton.hide();
    resetButton.show();
  }

  if (!play && lives == 0) {
    textSize(32);
    textAlign(CENTER);
    text("THE END", canvasWidth / 2, canvasHeight / 3 - size / 1.5);
  }

  ballBounceofPallet();
  ballBoundries();
  // autoPlay();

  if (smileyFaceStepY >= canvasHeight) {
    lives--;
    defaultPos();
    playPause();
  }
  bricksWall.forEach((row) => {
    row.forEach((brick) => {
      brick.drawBrick();
    });
  });

  checkBrick();
  moveBall();
}

function bricksWallRow(y) {
  bricksWallCurr = [];
  bricksPosX = 20;
  fill("red");
  for (j = 0; j < canvasWidth / 2; j++) {
    if (bricksPosX < canvasWidth - enemyBrickSize) {
      bricksWallCurr[j] = new enemyBrick(bricksPosX, y, enemyBrickSize, 15);
      bricksPosX += enemyBrickSize * 1.5;
    }
  }
  bricksWall.push(bricksWallCurr);
  getPoints += bricksWallCurr.length;
}

function checkBrick() {
  // for (i = 0; i <= bricksWall.length; i++) {
  i = 0;
  for (j = bricksWallCurr.length - 1; j >= 0; j--) {
    if (RectCircleColliding(ball, bricksWallCurr[j])) {
      smileyFaceStepTurnY *= -1;
      bricksWallCurr.splice(j, 1);
      points++;
    }
  }
  // }
}

class enemyBrick {
  constructor(x, y, w, h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.hit = false;
  }

  drawBrick() {
    if (!this.hit) {
      rect(this.x, this.y, this.w, this.h);
    }
  }
}

function heartsLeft() {
  fill("red");
  heartSize = 20;
  pos = 0;
  for (i = 0; i < lives; i++) {
    pos += heartSize * 1.5;
    heart(canvasWidth - pos, canvasHeight / 40, heartSize);
  }
}
function heart(x, y, size) {
  beginShape();
  vertex(x, y);
  bezierVertex(x - size / 2, y - size / 2, x - size, y + size / 3, x, y + size);
  bezierVertex(x + size, y + size / 3, x + size / 2, y - size / 2, x, y);
  endShape(CLOSE);
}
function defaultPos() {
  smileyFaceStepTurnX =
    ballSpeed * (Math.floor(Math.random() * Math.floor(2)) == 1 ? 1 : -1);
  smileyFaceStepTurnY =
    ballSpeed * (Math.floor(Math.random() * Math.floor(2)) == 1 ? 1 : -1);
  smileyFaceStepX = Math.floor(canvasWidth / 2);
  smileyFaceStepY = Math.floor(canvasHeight * 0.8 - ((canvasHeight * 0.8) % 5));
}

function ballBoundries() {
  //wall ball boundries/ bounce
  if (
    smileyFaceStepX >= canvasWidth - size / 2 ||
    smileyFaceStepX <= size / 2
  ) {
    smileyFaceStepTurnX *= -1;
  }
  if (smileyFaceStepY <= size / 2) {
    smileyFaceStepTurnY *= -1;
  }
}

function moveBall() {
  if (play) {
    smileyFaceStepX += smileyFaceStepTurnX;
    smileyFaceStepY += smileyFaceStepTurnY;
  }
}

function ballBounceofPallet() {
  //get pos of x on both sides of palett
  // mouseXPos = isAuto ? smileyFaceStepX : mouseX + palletSize / 2;
  // mouseXPosTwo = isAuto ? smileyFaceStepX : mouseX - palletSize / 2;

  // //pos of palett bounce if mouse is not on screen(left)
  // if (mouseX >= canvasWidth - palletSize) {
  //   mouseXPos = canvasWidth;
  //   mouseXPosTwo = canvasWidth - palletSize;
  // }

  // //pos of palett bounce if mouse is not on screen(right)
  // if (mouseX <= palletSize / 2) {
  //   mouseXPos = palletSize;
  //   mouseXPosTwo = 0;
  // }
  //ball bounce on pos of x of palette
  if (
    RectCircleColliding(ball, {
      x:
        isAuto == 1
          ? smileyFaceStepX - palletSize / 2
          : mouseX - palletSize / 2,
      y: palletHeight,
      w: palletSize,
      h: 15,
    })
  ) {
    smileyFaceStepTurnY *= -1;
  }
}
function RectCircleColliding(circle, rect) {
  var distX = Math.abs(circle.x - rect.x - rect.w / 2);
  var distY = Math.abs(circle.y - rect.y - rect.h / 2);

  if (distX > rect.w / 2 + circle.r) {
    return false;
  }
  if (distY > rect.h / 2 + circle.r) {
    return false;
  }

  if (distX <= rect.w / 2) {
    return true;
  }
  if (distY <= rect.h / 2) {
    return true;
  }

  var dx = distX - rect.w / 2;
  var dy = distY - rect.h / 2;
  return dx * dx + dy * dy <= circle.r * circle.r;
}

function autoPlay() {
  isAuto = 1;
}

function playPause() {
  if (play) {
    play = false;
  } else play = true;
}

function palett() {
  fill("blue");
  x = isAuto == 1 ? smileyFaceStepX - palletSize / 2 : mouseX - palletSize / 2;
  if (x + palletSize / 2 <= palletSize / 2) {
    x = 0;
  } else if (x + palletSize / 2 >= canvasWidth - palletSize / 2) {
    x = canvasWidth - palletSize;
  }

  pallet = rect(x, palletHeight, palletSize, 15);
}

class smileyFace {
  constructor(x, y, r) {
    this.x = x;
    this.y = y;
    this.r = r / 2;
    fill("yellow");
    circle(x, y, r);
    fill("white");
    circle(x + r * 0.17, y - 10, (2 / 10) * r);
    circle(x - r * 0.15, y - 10, (2 / 10) * r);
    fill("green");
    circle(x + r * 0.17, y - 10, (1 / 10) * r);
    circle(x - r * 0.15, y - 10, (1 / 10) * r);
    fill(0);
    circle(x + r * 0.17, y - 10, (4 / 100) * r);
    circle(x - r * 0.15, y - 10, (4 / 100) * r);
    noFill();
    arc(x, y, r * 0.6, r * 0.6, 0.5, PI / 1.2);
  }
}
